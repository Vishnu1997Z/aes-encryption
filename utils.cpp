#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

//reads from a file
void get_data(const char *filename, char *data)
{
	FILE *fin = fopen(filename, "r");
	for (size_t i = 0; i < 1000000; ++i)
		fscanf(fin, "%c", &data[i]);
	fclose(fin);
}

//writes to a file
void put_data(const char *filename, const char *data,unsigned int length)
{
	FILE *fout = fopen(filename, "w");
	for (size_t i = 0; i < length; ++i)
		fprintf(fout, "%c", data[i]);
	fclose(fout);
}

#ifdef FILE_TEST

int main()
{
	char *data;
	data = (char *)malloc(sizeof(char) * 1000000);
	get_data("test.txt",data);
	//printf("%c%c%c%c",data[0],data[1],data[2],data[4]);
	put_data("pt.txt",data,1000000);
	free(data);
	return 0;
}

#endif