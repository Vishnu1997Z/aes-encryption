#ifndef UTILS_H
#define UTILS_H

//reads from a file
void get_data(const char* filename,char *data);
//writes to a file
void put_data(const char* filename,const char *data,unsigned int length);

#endif