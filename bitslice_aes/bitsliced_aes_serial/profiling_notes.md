#### Profiling code

To profile code using gprof use the following commands
```
./bitslice (add -pg flag in the Makefile)
gprof bitslice gmon.out > bs_profile_output.txt
```


