#include <stdio.h>
#include <stdlib.h>

//creates a test file
void create_test()
{
	FILE *fout = fopen("test.txt", "w");
	for (size_t i = 0; i < 1000000; ++i)
	{
		int c = rand()%127;
		if(c<32)
			--i;
		else
		fprintf(fout, "%c", (char)c);
	}

	fclose(fout);
}

#ifdef TEST
int main()
{
	create_test();
	return 0;
}
#endif