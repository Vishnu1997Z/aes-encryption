#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>
#include<inttypes.h>
#include<memory.h>
#include<stdlib.h>

const uint32_t N = 4; // Number of 32-bit words in key length.
const uint32_t R = 11; // Number of round keys required.

// Round Constants.
const uint32_t rCon[] = {0x00000000, 0x01000000, 0x02000000, 0x04000000, 0x08000000, 0x10000000, 
                        0x20000000, 0x40000000, 0x80000000, 0x1B000000, 0x36000000};

// Key stored in groups of 32-bit words.
const uint32_t key[N] = {0x2B7E1516, 0x28AED2A6, 0xABF71588, 0x09CF4F3C};
//const uint32_t key[N] = {0x00000000, 0x00000000, 0x00000000, 0x00000000};

// S-Box for SubBytes and SubWord.
const uint8_t sbox[256] = {
  //0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
  0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
  0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
  0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
  0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
  0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
  0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
  0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
  0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
  0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
  0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
  0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
  0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
  0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
  0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
  0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
  0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 };

// Expanded round keys (128 bits).
uint32_t W[4 * R];

// Circularly rotates k 8 bytes left.  
uint32_t RotWord(uint32_t k)
{
    uint32_t x = (k / 0x01000000);
    k <<= 8;
    k += x;
    return k;
}

// Substitutes k wordwise with its corresponding S-Box elements.
uint32_t SubWord(uint32_t k)
{
    uint32_t ans = 0;
    uint8_t x;

    x = (k / 0x01000000);
    ans += sbox[x];
    k <<= 8;

    x = k / 0x01000000;
    ans *= 256;
    ans += sbox[x];
    k <<= 8;

    x = k / 0x01000000;
    ans *= 256;
    ans += sbox[x];
    k <<= 8;

    x = k / 0x01000000;
    ans *= 256;
    ans += sbox[x];

    return ans;
}

// Initializes the expanded round keys.
void initKeySchedule()
{
    for (uint8_t i = 0; i < 4 * R; i++)
    {
        //printf("%u ", i);
        if (i < N)
        {
            W[i] = key[i];
            //printf("%x ", W[i]);
        }

        else if (i >= N && i % N == 0)
        {
            W[i] = W[i - N] ^ SubWord(RotWord(W[i - 1])) ^ rCon[i / N];
            //printf("%x ", W[i]);
        }

        else if (i >= N && N > 6 && i % N == 4)
        {
            W[i] = W[i - N] ^ SubWord(W[i - 1]);
            //printf("%x ", W[i]);
        }

        else
        {
            W[i] = W[i - N] ^ W[i - 1];
            //printf("%x ", W[i]);
        }

        // if (i % 4 == 3)
        //     printf("\n");
        
    }
}

// Performes multiplication in GF(2^8).
uint8_t multiply(uint8_t k, uint8_t x)
{
    if (k == 2)
    {
        if (x >> 7 == 0)
            return (x << 1);
        else
            return (x << 1) ^ 0x1B;
    }

    else if (k == 3)
    {
        if (x >> 7 == 0)
            return (x << 1) ^ x;
        else
            return (x << 1) ^ 0x1B ^ x;
    }

    else
    {
        return x;
    }
}

// XOR with Round Key.
uint8_t* AddRoundKey(uint8_t *input, uint8_t round)
{
    for (uint8_t i = 0; i < 4; i++)
    {
        uint32_t x = 0;

        x += input[4 * i];
        x *= 256;
        x += input[4 * i + 1];
        x *= 256;
        x += input[4 * i + 2];
        x *= 256; 
        x += input[4 * i + 3];

        x ^= W[round * 4 + i];

        input[4 * i + 3] = x % 256;
        x /= 256;
        input[4 * i + 2] = x % 256;
        x /= 256;
        input[4 * i + 1] = x % 256;
        x /= 256;
        input[4 * i] = x;
    }

    return input;
}

// Performs S-Box substitiution.
uint8_t* SubBytes(uint8_t *input)
{
    for (uint8_t i = 0; i < 16; i++)
    {
        input[i] = sbox[input[i]];
    }

    return input;
}

// Shifts rows as required by the AES algorithm.
uint8_t* ShiftRows(uint8_t *input)
{
    for (uint8_t i = 1; i < 4; i++)
    {
        uint32_t temp1, temp2;

        if (i == 1)
        {
            temp1 = input[i];
            input[i] = input[4 + i];
            input[4 + i] = input[8 + i];
            input[8 + i] = input[12 + i];
            input[12 + i] = temp1;
        }

        else if (i == 2)
        {
            temp1 = input[i];
            temp2 = input[4 + i];
            input[i] = input[8 + i];
            input[4 + i] = input[12 + i];
            input[8 + i] = temp1;
            input[12 + i] = temp2;
        }

        else
        {
            temp1 = input[i];
            input[i] = input[12 + i];
            input[12 + i] = input[8 + i];
            input[8 + i] = input[4 + i];
            input[4 + i] = temp1;
        }
    }

    return input;
}

// Implements MixColumns in GF(2^8).
uint8_t* MixColumns(uint8_t *input)
{
    uint8_t temp[4];
    for (uint8_t i = 0; i < 4; i++)
    {
        temp[0] = input[4 * i];
        temp[1] = input[4 * i + 1];
        temp[2] = input[4 * i + 2];
        temp[3] = input[4 * i + 3];

        input[4 * i] = multiply(2, temp[0]) ^ multiply(3, temp[1]) ^ multiply(1, temp[2]) ^ multiply(1, temp[3]);
        input[4 * i + 1] = multiply(1, temp[0]) ^ multiply(2, temp[1]) ^ multiply(3, temp[2]) ^ multiply(1, temp[3]);
        input[4 * i + 2] = multiply(1, temp[0]) ^ multiply(1, temp[1]) ^ multiply(2, temp[2]) ^ multiply(3, temp[3]);
        input[4 * i + 3] = multiply(3, temp[0]) ^ multiply(1, temp[1]) ^ multiply(1, temp[2]) ^ multiply(2, temp[3]);
    }

    return input;
}

// Writes output to file.
void writeOutput(FILE *f, uint8_t *output)
{
    fwrite(output, 16, 1, f);    
}

// Performs the actual AES encryption.
uint8_t* AESEncrypt(uint8_t *input)
{
    input = AddRoundKey(input, 0);
    // printInput(input);

    for (uint8_t i = 1; i < R - 1; i++)
    {
        input = SubBytes(input);
        // if (i == 1)
        //     printInput(input);
        input = ShiftRows(input);
        // if (i == 1)
        //     printInput(input);
        input = MixColumns(input);
        // if (i == 1)
        //     printInput(input);
        input = AddRoundKey(input, i);
        // if (i == 1)
        //     printInput(input);
    }

    input = SubBytes(input);
    input = ShiftRows(input);
    input = AddRoundKey(input, R - 1);

    return input;
}

int main()
{
    uint8_t *input = (uint8_t*)malloc(128);
    input[0] = 0x6B;
    input[1] = 0xC1;
    input[2] = 0xBE;
    input[3] = 0xE2;
    input[4] = 0x2E;
    input[5] = 0x40;
    input[6] = 0x9F;
    input[7] = 0x96;
    input[8] = 0xE9;
    input[9] = 0x3D;
    input[10] = 0x7E;
    input[11] = 0x11;
    input[12] = 0x73;
    input[13] = 0x93;
    input[14] = 0x17;
    input[15] = 0x2A; 

    FILE *fr = fopen("input.txt", "rb");
    if (fr == NULL)
    {
        printf("Cannot open input file...Exiting");
        exit(0);
    }
    FILE *fw = fopen("output.txt", "wb");
    if (fw == NULL)
    {
        printf("Cannot open output file...Exiting");
        exit(0);
    }
    
    size_t s = 0;

    initKeySchedule();

    while (true)
    {
        s = fread(input, 1, 16, fr);
        if (s == 16)
        {
            //printf("a\n");
            input = AESEncrypt(input);
            writeOutput(fw, input);
            //for (uint8_t i = 0; i < 16; i++)
              //  printf("%02x ", input[i]);
            //printf("\n");
        }
        // Handles the edge case for the last batch of input.
        else
        {
            if (s == 0)
                break;
            //printf("b\n");
            memset(input + s, 0x00, 16 - s);
            input = AESEncrypt(input);
            //for (uint8_t i = 0; i < 16; i++)
              //  printf("%02x ", input[i]);
            //printf("\n");
            writeOutput(fw, input);
            break;
        }
    }

    //printInput(input);

    free(input);

    fclose(fr);
    fclose(fw);

    return 0;
}