#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>
#include<inttypes.h>
#include<memory.h>
#include<time.h>
#include<cuda.h>
#include<cuda_runtime_api.h>

const uint32_t N = 4; // Number of 32-bit words in key length.
const uint32_t R = 11; // Number of round keys required.

// Round Constants.
const uint32_t rCon[] = {0x00000000, 0x01000000, 0x02000000, 0x04000000, 0x08000000, 0x10000000, 
                        0x20000000, 0x40000000, 0x80000000, 0x1B000000, 0x36000000};

// Key stored in groups of 32-bit words.
const uint32_t key[N] = {0x2B7E1516, 0x28AED2A6, 0xABF71588, 0x09CF4F3C};

// S-Box for SubBytes and SubWord.
const uint8_t sbox[256] = {
  //0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
  0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
  0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
  0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
  0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
  0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
  0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
  0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
  0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
  0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
  0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
  0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
  0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
  0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
  0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
  0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
  0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 };

const uint8_t diffMatrix[16] = {2, 3, 1, 1, 1, 2, 3, 1, 1, 1, 2, 3, 3, 1, 1, 2};

// __constant__ uint8_t diffMatrixDevice[16];

// Expanded round keys (128 bits).
uint32_t W[4 * R];
uint8_t W_small[4 * 4 * R];

// __constant__ uint8_t sboxDevice[256];
// __constant__ uint32_t WDevice[4 * R];

// Circularly rotates k 8 bytes left.  
uint32_t RotWord(uint32_t k)
{
    uint32_t x = (k / 0x01000000);
    k <<= 8;
    k += x;
    return k;
}

// Substitutes k wordwise with its corresponding S-Box elements.
uint32_t SubWord(uint32_t k)
{
    uint32_t ans = 0;
    uint8_t x;

    x = (k / 0x01000000);
    ans += sbox[x];
    k <<= 8;

    x = k / 0x01000000;
    ans *= 256;
    ans += sbox[x];
    k <<= 8;

    x = k / 0x01000000;
    ans *= 256;
    ans += sbox[x];
    k <<= 8;

    x = k / 0x01000000;
    ans *= 256;
    ans += sbox[x];

    return ans;
}

// Initializes the expanded round keys.
void initKeySchedule()
{
    for (uint32_t i = 0; i < 4 * R; i++)
    {
        //printf("%u ", i);
        if (i < N)
            W[i] = key[i];

        else if (i >= N && i % N == 0)
            W[i] = W[i - N] ^ SubWord(RotWord(W[i - 1])) ^ rCon[i / N];

        else if (i >= N && N > 6 && i % N == 4)
            W[i] = W[i - N] ^ SubWord(W[i - 1]);

        else
            W[i] = W[i - N] ^ W[i - 1];

        // printf("%08x ", W[i]);

        uint32_t x = W[i];
        W_small[4 * i + 3] = x % 256;
        x /= 256;
        W_small[4 * i + 2] = x % 256;
        x /= 256;
        W_small[4 * i + 1] = x % 256;
        x /= 256;
        W_small[4 * i] = x % 256; 
        
    }
    // printf("\n");
}

// Performes multiplication in GF(2^8).
__device__ uint8_t DeviceMultiply(uint8_t k, uint8_t x)
{
    if (k == 2)
    {
        if (x >> 7 == 0)
            return (x << 1);
        else
            return (x << 1) ^ 0x1B;
    }

    else if (k == 3)
    {
        if (x >> 7 == 0)
            return (x << 1) ^ x;
        else
            return (x << 1) ^ 0x1B ^ x;
    }

    else
    {
        return x;
    }
}

// XOR with Round Key.
__global__ void DeviceAddRoundKey(uint8_t *input, uint8_t round, uint8_t *W_small, size_t fileSize)
{
    uint32_t threadId = blockIdx.x * blockDim.x + threadIdx.x;
    // uint32_t offset = threadId * 16;

    if (threadId < fileSize)
    {
        uint32_t index = threadId % 16;
        // for (uint8_t i = 0; i < 4; i++)
        // {
        //     uint32_t x = 0;

        //     x += input[offset + 4 * i];
        //     x *= 256;
        //     x += input[offset + 4 * i + 1];
        //     x *= 256;
        //     x += input[offset + 4 * i + 2];
        //     x *= 256; 
        //     x += input[offset + 4 * i + 3];

        //     x ^= W[round * 4 + i];

        //     input[offset + 4 * i + 3] = x % 256;
        //     x /= 256;
        //     input[offset + 4 * i + 2] = x % 256;
        //     x /= 256;
        //     input[offset + 4 * i + 1] = x % 256;
        //     x /= 256;
        //     input[offset + 4 * i] = x;
        // }
        input[threadId] ^= W_small[16 * round + index];
    }
}

// Performs S-Box substitiution.
__global__ void DeviceSubBytes(uint8_t *input, uint8_t *sboxDevice, size_t fileSize)
{
    uint32_t threadId = blockIdx.x * blockDim.x + threadIdx.x;

    if (threadId < fileSize)
        input[threadId] = sboxDevice[input[threadId]];
}

__global__ void DeviceShiftRows2(uint8_t *input, size_t fileSize)
{
    uint32_t threadId = blockIdx.x * blockDim.x + threadIdx.x;
    uint32_t offset = threadId * 16;

    if (threadId < fileSize / 16)
    {
        uint32_t temp1;
        temp1 = input[offset + 1];
        input[offset + 1] = input[offset + 5];
        input[offset + 5] = input[offset + 9];
        input[offset + 9] = input[offset + 13];
        input[offset + 13] = temp1;
    }
}

__global__ void DeviceShiftRows3(uint8_t *input, size_t fileSize)
{
    uint32_t threadId = blockIdx.x * blockDim.x + threadIdx.x;
    uint32_t offset = threadId * 16;

    if (threadId < fileSize / 16)
    {
        uint32_t temp1, temp2;
        temp1 = input[offset + 2];
        temp2 = input[offset + 6];
        input[offset + 2] = input[offset + 10];
        input[offset + 6] = input[offset + 14];
        input[offset + 10] = temp1;
        input[offset + 14] = temp2;
    }
}

__global__ void DeviceShiftRows4(uint8_t *input, size_t fileSize)
{
    uint32_t threadId = blockIdx.x * blockDim.x + threadIdx.x;
    uint32_t offset = threadId * 16;

    if (threadId < fileSize / 16)
    {
        uint32_t temp1;
        temp1 = input[offset + 3];
        input[offset + 3] = input[offset + 15];
        input[offset + 15] = input[offset + 11];
        input[offset + 11] = input[offset + 7];
        input[offset + 7] = temp1;
    }
}

// Shifts rows as required by the AES algorithm.
__global__ void DeviceShiftRows(uint8_t *input, size_t fileSize)
{
    uint32_t threadId = blockIdx.x * blockDim.x + threadIdx.x;
    uint32_t offset = threadId * 16;

    if (offset < fileSize)
    {
        for (uint8_t i = 1; i < 4; i++)
        {
            uint32_t temp1, temp2;

            if (i == 1)
            {
                temp1 = input[offset + i];
                input[offset + i] = input[offset + 4 + i];
                input[offset + 4 + i] = input[offset + 8 + i];
                input[offset + 8 + i] = input[offset + 12 + i];
                input[offset + 12 + i] = temp1;
            }

            else if (i == 2)
            {
                temp1 = input[offset + i];
                temp2 = input[offset + 4 + i];
                input[offset + i] = input[offset + 8 + i];
                input[offset + 4 + i] = input[offset + 12 + i];
                input[offset + 8 + i] = temp1;
                input[offset + 12 + i] = temp2;
            }

            else
            {
                temp1 = input[offset + i];
                input[offset + i] = input[offset + 12 + i];
                input[offset + 12 + i] = input[offset + 8 + i];
                input[offset + 8 + i] = input[offset + 4 + i];
                input[offset + 4 + i] = temp1;
            }
        }
    }
}

// Implements MixColumns in GF(2^8).
__global__ void DeviceMixColumns(uint8_t *input, size_t fileSize, uint8_t *diffMatrixDevice)
{
    uint32_t threadId = blockIdx.x * blockDim.x + threadIdx.x;
    // uint32_t offset = threadId * 16;

    __shared__ uint8_t temp[16];

    if (threadId < fileSize)
    {
        // for (uint8_t i = 0; i < 4; i++)
        // {
        //     temp[0] = input[offset + 4 * i];
        //     temp[1] = input[offset + 4 * i + 1];
        //     temp[2] = input[offset + 4 * i + 2];
        //     temp[3] = input[offset + 4 * i + 3];

        //     input[offset + 4 * i] = DeviceMultiply(2, temp[0]) ^ DeviceMultiply(3, temp[1]) ^ DeviceMultiply(1, temp[2]) ^ DeviceMultiply(1, temp[3]);
        //     input[offset + 4 * i + 1] = DeviceMultiply(1, temp[0]) ^ DeviceMultiply(2, temp[1]) ^ DeviceMultiply(3, temp[2]) ^ DeviceMultiply(1, temp[3]);
        //     input[offset + 4 * i + 2] = DeviceMultiply(1, temp[0]) ^ DeviceMultiply(1, temp[1]) ^ DeviceMultiply(2, temp[2]) ^ DeviceMultiply(3, temp[3]);
        //     input[offset + 4 * i + 3] = DeviceMultiply(3, temp[0]) ^ DeviceMultiply(1, temp[1]) ^ DeviceMultiply(1, temp[2]) ^ DeviceMultiply(2, temp[3]);
        // }
        int index = threadId % 16;
        temp[index] = input[threadId];
        __syncthreads();
        int row = index / 4;
        int col = index % 4;
        input[threadId] = DeviceMultiply(diffMatrixDevice[col * 4], temp[row * 4]) ^ DeviceMultiply(diffMatrixDevice[col * 4 + 1], temp[row * 4 + 1])
                            ^ DeviceMultiply(diffMatrixDevice[col * 4 + 2], temp[row * 4 + 2]) ^ DeviceMultiply(diffMatrixDevice[col * 4 + 3], temp[row * 4 + 3]);
    }
}

int main()
{
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    size_t fileSize = 32;

    uint8_t *input = (uint8_t*)malloc(fileSize * sizeof(uint8_t));
    uint8_t *output = (uint8_t*)malloc(fileSize * sizeof(uint8_t));

    uint8_t *d_input;
    cudaMalloc(&d_input, fileSize * sizeof(uint8_t));

    FILE *fr = fopen("input1.txt", "rb");
    if (fr == NULL)
    {
        printf("Cannot open input file...Exiting");
        exit(0);
    }
    FILE *fw = fopen("output.txt", "wb");
    if (fw == NULL)
    {
        printf("Cannot open output file...Exiting");
        exit(0);
    }

    initKeySchedule();

    uint8_t *WDevice;
    uint8_t *sboxDevice;
    uint8_t *diffMatrixDevice;
    cudaMalloc(&WDevice, 4 * 4 * R * sizeof(uint8_t));
    cudaMalloc(&sboxDevice, 256 * sizeof(uint8_t));
    cudaMalloc(&diffMatrixDevice, 16 * sizeof(uint8_t));

    cudaMemcpy(sboxDevice, sbox, 256 * sizeof(uint8_t), cudaMemcpyHostToDevice);
    cudaMemcpy(WDevice, W_small, 4 * 4 * R * sizeof(uint8_t), cudaMemcpyHostToDevice);
    cudaMemcpy(diffMatrixDevice, diffMatrix, 16 * sizeof(uint8_t), cudaMemcpyHostToDevice);

    fread(input, 1, fileSize, fr);

    cudaMemcpy(d_input, input, fileSize * sizeof(uint8_t), cudaMemcpyHostToDevice);

    cudaStream_t s1, s2, s3;
    cudaStreamCreate(&s1);
    cudaStreamCreate(&s2);
    cudaStreamCreate(&s3);

    cudaEventRecord(start);
    DeviceAddRoundKey<<<4, 8>>>(d_input, 0, WDevice, fileSize);

    for (uint8_t j = 1; j < R - 1; j++)
    {
        DeviceSubBytes<<<4, 8>>>(d_input, sboxDevice, fileSize);
        DeviceShiftRows2<<<1, 2, 0, s1>>>(d_input, fileSize);
        DeviceShiftRows3<<<1, 2, 0, s2>>>(d_input, fileSize);
        DeviceShiftRows4<<<1, 2, 0, s3>>>(d_input, fileSize);
        DeviceMixColumns<<<2, 16>>>(d_input, fileSize, diffMatrixDevice);
        DeviceAddRoundKey<<<4, 8>>>(d_input, j, WDevice, fileSize);
    }

    DeviceSubBytes<<<4, 8>>>(d_input, sboxDevice, fileSize);
    DeviceShiftRows2<<<1, 2, 0, s1>>>(d_input, fileSize);
    DeviceShiftRows3<<<1, 2, 0, s2>>>(d_input, fileSize);
    DeviceShiftRows4<<<1, 2, 0, s3>>>(d_input, fileSize);
    DeviceAddRoundKey<<<4, 8>>>(d_input, R - 1, WDevice, fileSize);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    cudaStreamDestroy(s1);
    cudaStreamDestroy(s2);
    cudaStreamDestroy(s3);

    float ms = 0.0;
    cudaEventElapsedTime(&ms, start, stop);
    printf("%f\n", ms);

    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    cudaMemcpy(output, d_input, fileSize * sizeof(uint8_t), cudaMemcpyDeviceToHost);

    for (uint32_t i = 0; i < fileSize; i++)
        printf("%02x ", output[i]);

    fwrite(output, 1, fileSize, fw);

    free(input);

    cudaFree(d_input);
    cudaFree(sboxDevice);
    cudaFree(WDevice);
    cudaFree(diffMatrixDevice);

    fclose(fr);
    fclose(fw);

    return 0;
}