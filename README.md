### Introduction
+ The aim of this project is to parallelize the AES encryption algorithm as a part of the CS6023 : Introduction to GPU Programming at IIT Madras.
+ AES is a symmetric key block cipher which works on blocks of size 16 bytes each. It offers a security of 128, 192 and 256 bits respectively wherein each implementation consists of 10, 12 and 14 rounds each. It has five modes of operation: ECB (Electronic Code Book), CBC (Cipher Block Chaining), CFB (Cipher FeedBack), OFB (Output FeedBack), and CTR (Counter).

### Organisation
+ The Vanilla serial and parallel implementation code is present in the AES_ECB directory.
+ The code corresponding to the t-table's serial and parallel implementation is present in the aes-t-table directory.
+ The bitslice, serial code and parallel versions are present in the bitslice_aes folder.
+ The utils.c file contains basic file operations and code to generate random input text files that are utilized by the other programs.
+ The test.txt is the standard 1 MB input that we benchmarked our code against.

### Execution
#### Serial Code
+ The serial code can be compiled as follows : 
```
gcc -o <executable> filename.c -O2 -pg
```
+ We can then run the executable using :
```
./<executable>
```
#### Parallel Code
+ The executable is generated using the following command :
```
nvcc filename.cu -o  <executable>
```
+ To run the executable :
```
./<executable>
```
